# Generate 1099 Files for Transmission Through the IRS FIRE System

Fire-1099-gen allows you to generate IRS FIRE-formatted files. Currently only 1099-NEC files are supported. This project is a completely re-written version of the project at https://github.com/sdj0/fire-1099.

Please note that to access the IRS FIRE system requires clearing a few hurdles ([overview here](https://www.irs.gov/e-file-providers/filing-information-returns-electronically-fire)). The first step is to create an [IR-TCC account](https://la.www4.irs.gov/secureaccess/ui) which will allow signing up for FIRE.

Also note that the new [IRIS system](https://www.irs.gov/filing/e-file-forms-1099-with-iris) might be easier to use, depending on your use-case.


# Using fire-1099-gen

## Overview

Form data is entered into JSON files which are validated and processed by the application. The output file (which contains a single string of ascii characters) can be uploaded through FIRE.


## Usage

1. Install the fire-1099-gen application:

    * Clone the repository.
    * Change to the repository's root directory.
    * Run `pipx install .`


2. Create a JSON file with your data (copy `data/Fire_1099_NEC_Data_Template.json` to get started). You can reference the schema in `schemas/` for specific details about the JSON format.

3. Generate the file thus:

    ```bash
    $ fire-1099 --generate -t NEC -i PATH/TO/INPUT_FILE.json -o PATH/TO/OUTPUT_FILE.ascii
    ```

    You will see a preview of the records generated in the output, and an ASCII file will be created that can be uploaded to FIRE.


# TODO

* Improve code comments and documentation.
* Add support for more file types (e.g. MISC).
* Add support for 'K' records.
* Add some tests.
* Migrate to pipenv?
