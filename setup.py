"""
fire_1099 module packaging
"""


from setuptools import setup, find_packages

setup(
    name="fire-1099-gen",
    version="0.0.1-beta",
    description="Generate 1099 files for transmission through the \
    IRS FIRE system",
    long_description=open("README.md").read(),
    license="GPLv3",

    author="Brooks Kline",
    author_email="brookskline@caiyos.com",
    url="https://bitbucket.org/brelkl/fire-1099-gen",

    packages=find_packages(exclude=["docs", "tests*"]),
    include_package_data=True,
    install_requires=["jsonschema"],
    entry_points={
        "console_scripts": [
            "fire-1099=fire_1099.commands:cli"
        ],
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Natural Language :: English",
        "Intended Audience :: End Users/Desktop",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3"
    ]
)
