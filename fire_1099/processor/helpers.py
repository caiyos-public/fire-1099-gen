"""
Module: processor.helpers

Helper functions used by fire-1099-gen.
"""


import re


def digits_only(value):

    """
    Remove all non-numeric characters
    """

    return re.sub("[^0-9]*", "", value)


def ljust_null_fill(value, length):

    """
    Left-justify value and pad with '\x00' to 'length'
    """

    null_char = "\x00"

    return f"{value:{null_char}<{length}}"


def ljust_num_only_null_fill(value, length):

    """
    Combine ljust_null_fill and digits_only
    """

    return ljust_null_fill(digits_only(value), length)


def rjust_zero_fill(value, length):

    """
    Right-justify, remove all non-numeric characters,
    pad with zeros to 'length'
    """

    value = str(value)
    return f"{digits_only(value):0>{length}}"
