"""
Module: processor.core

Processes JSON files into an IRS 1099 FIRE-formatted files.
Currently only 1099-NEC files are supported.
"""


import sys
import logging
from pathlib import Path
import json
from jsonschema import validate
import fire_1099.processor.helpers as helpers


# configure logging
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


class FireFile:

    """
    A class to build and process a fire-formatted 1099 file
    """

    _payment_codes = {
        "all": ("1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","J"),
        "NEC": ("1", "4"),
        "MISC": ("1","2","3","4","5","6","8","A","B","C","D","E","F","G")
    }

    _field_lengths = {
        "record_sequence_number": 8,
        "amount_codes": 18,
        "number_of_payees": 8,
        "number_of_a_records": 8,
        "payment_amount": 18
    }


    def __init__(self, file_type, preview_type="short"):

        self._file_type = file_type
        self._preview_type = preview_type
        self._transformed_data = {
            "transmitter": {},
            "issuers": [],
            "end_of_transmission": {}
        }
        self._record_counter = 0
        self._issuer_counter = 0
        self._payee_counter = 0
        self._ascii_output = ""


    def get_next_rec_num(self):

        self._record_counter += 1
        _length = self._field_lengths["record_sequence_number"]

        return f"{self._record_counter:0>{_length}}"


    def process_data(self, source_file):

        _schema_file = "1099_" + self._file_type + "_schema.json"
        _schema_path = Path(__file__).parent.joinpath(
            "../schemas/", _schema_file).resolve()

        if not _schema_path.exists():
            logging.exception("Schema file '" + _schema_file + "' not found; exiting")
            raise SystemExit(1)

        with open(source_file, "r", encoding="utf-8") as source_json:

            try:
                _user_data = json.load(source_json)

            except json.JSONDecodeError:
                logging.exception("There seem to be formatting issues with the JSON file: '" +
                                  source_file.name + "'; exiting.")
                raise SystemExit(1)

        with open(_schema_path, "r", encoding="utf-8") as schema:

            try:
                _schema_data = json.load(schema)

            except json.decoder.JSONDecodeError:
                logging.exception("There was an error parsing the JSON schema: '" +
                                  source_json.name + "'; exiting.\n")
                raise SystemExit(1)

        validate(_user_data, _schema_data)

        # create record templates from schema
        t_transmitter = _schema_data["properties"]["transmitter"]["properties"]
        t_issuer = _schema_data["properties"]["issuers"]["items"]["properties"]
        t_payee = t_issuer.pop("payees")["items"]["properties"]
        t_end_of_issuer = t_issuer.pop("end_of_issuer")["properties"]
        t_end_of_transmission = _schema_data["properties"]["end_of_transmission"]["properties"]

        # process user data
        for record in _user_data:

            if record == "transmitter":

                # process T record based on template
                self._transformed_data["transmitter"] = \
                    self.process_record(t_transmitter, _user_data[record])

            elif record == "issuers":

                for issuer in _user_data[record]:

                    # process A records based on template
                    issuer_record = self.process_record(t_issuer, issuer)

                    # process B records based on template

                    # create placeholder
                    issuer_record["payees"] = []

                    for payee in issuer["payees"]:

                        issuer_record["payees"].append(self.process_record(t_payee, payee))

                    # process C record based on template
                    issuer_record["end_of_issuer"] = self.process_record(t_end_of_issuer, {})

                    # process issuer totals
                    self.process_issuer_totals(issuer_record)

                    # append issuer to main dict
                    self._transformed_data["issuers"].append(issuer_record)

        # process F record based on template
        self._transformed_data["end_of_transmission"] = self.process_record(t_end_of_transmission, {})

        # insert file totals
        self._transformed_data["transmitter"]["total_number_of_payees"] = \
            helpers.rjust_zero_fill(self._payee_counter, self._field_lengths["number_of_payees"])
        self._transformed_data["end_of_transmission"]["number_of_a_records"] = \
            helpers.rjust_zero_fill(self._issuer_counter, self._field_lengths["number_of_a_records"])

        # preview
        self.print_fire_file_preview(self._preview_type)


    def process_record(self, template, user_record):

        _processed_record = {}

        for field in template:

            # process value
            current_value = self.process_field_value(template, user_record, field)

            # build record
            _processed_record[field] = current_value

        _processed_record["record_sequence_number"] = self.get_next_rec_num()

        return _processed_record


    def process_field_value(self, template, record, field):

        # get template params for field
        _default, _length, _transform = self.get_template_params(template[field])

        # populate current value
        _value = self.populate_field(record, field, _default)

        # common transforms
        _value = self.apply_transforms(_value, field, _length, _transform)

        return _value


    def get_template_params(self, field_template):

        # these are global for all records
        _length = _value = _transform = None

        # get metadata
        if "description" in field_template:
            _metadata = dict(i.split("=") for i in field_template["description"].split(","))
            if "xfunc" in _metadata:
                _transform = _metadata["xfunc"]
            if "length" in _metadata:
                _length = _metadata["length"]

        if "maxLength" in field_template and _length is None:
            _length = field_template["maxLength"]

        if "default" in field_template and not "const" in field_template:
            _value = field_template["default"]

        # const takes precedence over 'default'
        if "const" in field_template:
            _value = field_template["const"]

        return (_value, _length, _transform)


    def populate_field(self, user_record, field, default):

        # if user data has the field and a value use it,
        # otherwise use template default

        if field in user_record:
            _current_value = user_record[field]

        else:
            _current_value = default

        return _current_value


    def apply_transforms(self, value, field, length, transform):

        # uppercase all strings (ignore null values and email)
        if (type(value) == str
            and value != "NUL"
            and not "email" in field):
            value = value.upper()

        # check for, and apply custom transforms
        if transform is not None:

            try:
                transform = getattr(helpers, transform)
                value = transform(value, length)

            except AttributeError:
                logging.exception(f"function not found '{transform}' (helpers); exiting.\n")

        # check for values longer than allowed length
        if (value != "NUL"
            and value is not None
            and len(value) > int(length)):
            raise ValueError(f"The feild '{field}' is longer than the allowed length; exiting.\n")

        # process NUL/empty values
        if value == "NUL" or value is None:
            value = "\x00" * length

        # default field process is ljust with null padding
        elif len(value) != length:
            value = helpers.ljust_null_fill(value, length)

        return value


    def validate_record(self, ascii_record, record_type, record_number):

        if len(ascii_record) != 750:
            _actual_length = len(ascii_record)
            raise Exception(f"'{record_type}' record {record_number} has an incorrect length ({_actual_length}).")


    def get_record_id(self, record):

        return record["record_type"], record["record_sequence_number"]


    def process_issuer_totals(self, issuer):

        _payment_codes = self._payment_codes[self._file_type]
        _amount_codes_length = self._field_lengths["amount_codes"]
        _number_of_payees_length = self._field_lengths["number_of_payees"]
        _payment_amount_length = self._field_lengths["payment_amount"]
        _payee_count = len(issuer["payees"])

        # init a blank list of values based on the number of codes
        _totals = [ 0 for _ in range(len(_payment_codes)) ]
        _amount_codes_string = ""

        # build totals
        for payee in issuer["payees"]:

            for i, code in enumerate(_payment_codes):

                try:
                    _totals[i] += int(payee["payment_amount_" + code])

                except ValueError:
                    pass

        # build 'amount_codes', insert totals in 'C' record
        for total, code in zip(_totals, _payment_codes):

            if total != 0:
                _amount_codes_string += code
                issuer["end_of_issuer"]["payment_amount_" + code] = \
                    helpers.rjust_zero_fill(total, _payment_amount_length)

        # populate fields
        # if a correctoion with no payment amounts is filed
        # use user value; otherwise...
        if _amount_codes_string != "":
            issuer["amount_codes"] = \
                helpers.ljust_null_fill(str(_amount_codes_string), _amount_codes_length)

        issuer["end_of_issuer"]["number_of_payees"] = \
            helpers.rjust_zero_fill(_payee_count, _number_of_payees_length)
        self._payee_counter += _payee_count
        self._issuer_counter += 1


    def export_fire_file(self, output_file):

        self.build_ascii_record(self._transformed_data["transmitter"])

        for issuer in self._transformed_data["issuers"]:

            self.build_ascii_record(issuer)

            for payee in issuer["payees"]:
                self.build_ascii_record(payee)

            self.build_ascii_record(issuer["end_of_issuer"])

        self.build_ascii_record(self._transformed_data["end_of_transmission"])

        # write file
        with open(output_file, "w+") as f:
            f.write(self._ascii_output)
            logging.info(f"File '{output_file}' successfully created.")


    def build_ascii_record(self, record):

        _current_record = ""

        for field, value in record.items():

            # skip sub-record lists
            if not (field == "issuers"
                    or field == "payees"
                    or field == "end_of_issuer"):

                _current_record += value

        _record_type, _record_number = self.get_record_id(record)
        self.validate_record(_current_record, _record_type, _record_number)
        self._ascii_output += _current_record


    def print_fire_file_preview(self, preview_type="short"):

        # preview_type="short|full"

        print("\nFIRE File Preview:")

        if preview_type == "short":
            _build_func = self.build_preview_record

        elif preview_type == "full":
            _build_func = self.build_full_preview_record

        _build_func(self._transformed_data["transmitter"])

        for issuer in self._transformed_data["issuers"]:

            _build_func(issuer)

            for payee in issuer["payees"]:
                _build_func(payee)

            _build_func(issuer["end_of_issuer"])

        _build_func(self._transformed_data["end_of_transmission"])

        print("=" * 40)
        print("End Preview\n")


    def build_full_preview_record(self, record):

        for field, value in record.items():

            # skip sub-record lists
            if not (field == "issuers"
                    or field == "payees"
                    or field == "end_of_issuer"):

                if field == "record_type":
                    print("=" * 40)

                value = value.replace("\x00", "\xb7")
                print(f"{field}: {str(value)}")


    def build_preview_record(self, record):

        for field, value in record.items():

            # skip sub-record lists
            if not (field == "issuers"
                    or field == "payees"
                    or field == "end_of_issuer"):

                if field == "record_type":
                    print("=" * 40)

                _nul_test = set(value)

                if not (len(_nul_test) == 1 and "\x00" in _nul_test):
                    print(f"{field}: {value}")


def create_fire_file(source_file, output_file, file_type, preview_type):

    """
    Main function
    """

    fire_file = FireFile(file_type, preview_type)
    fire_file.process_data(source_file)
    fire_file.export_fire_file(output_file)
