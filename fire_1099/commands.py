"""
fire-1099-gen entrypoint

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import sys
import logging
import argparse
from pathlib import Path
from fire_1099.processor.core import create_fire_file


# configure logging
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# supported 1099 file formats
format_types = ("NEC")


def cli():

    arg_parser=argparse.ArgumentParser(prog="fire-1099",
                                       description="Fire 1099 file generator")

    arg_parser.add_argument("--generate",
                            help="Generate a file (requires '-t', '-i', '-o')",
                            action="store_true")

    arg_parser.add_argument("-t",
                            choices=format_types,
                            default="NEC",
                            help="The 1099 file type",
                            type=str)

    arg_parser.add_argument("-i",
                            help="The JSON file to process",
                            type=str)

    arg_parser.add_argument("-o",
                            help="The 1099 file to create",
                            type=str)

    arg_parser.add_argument("-p",
                            help="Display a full previre (for debugging)",
                            action="store_true")

    args=arg_parser.parse_args()


    # parse args for 'generate' command

    if args.generate is True:

        if args.t is None or args.i is None or args.o is None:
            logging.exception("The 'generate' command is missing parameter(s); exiting.")
            raise SystemExit(1)

        else:
            # populate arg vars
            file_type = args.t
            source_file = args.i
            output_file = args.o
            preview_type = "full" if args.p else "short"

            # test params
            source_file = Path(source_file)
            if not source_file.exists() or not source_file.is_file():
                logging.exception("Cannot find the source file specified; exiting.")
                raise SystemExit(1)

            output_file = Path(output_file)
            if not output_file.parent.exists():
                logging.exception("Cannot find the output file directory specified; exiting.")
                raise SystemExit(1)

            # get absolute paths
            source_file = source_file.resolve()
            output_file = output_file.resolve()

            create_fire_file(source_file, output_file, file_type, preview_type)

    else:
       arg_parser.print_usage()
       arg_parser.exit(1, message="No command provided.\n")
